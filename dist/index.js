import pDelay from 'delay';
import pDebounce from 'p-debounce';
import pFilter from 'p-filter';
import pForever from 'p-forever';
import pIf from 'p-if';
import pLimit from 'p-limit';
import pLocate from 'p-locate';
import pMap from 'p-map';
import pMemoize from 'p-memoize';
import pPipe from 'p-pipe';
import pProgress from 'p-progress';
import pProps from 'p-props';
import pQueue from 'p-queue';
import pReduce from 'p-reduce';
import pReflect from 'p-reflect';
import pRetry from 'p-retry';
import pSome, { AggregateError, FilterError } from 'p-some';
import pTap from 'p-tap';
import pThrottle from 'p-throttle';
import pTime from 'p-time';
import pTimeout from 'p-timeout';
import pTimes from 'p-times';
import pWaitFor from 'p-wait-for';
import pWaterfall from 'p-waterfall';
import pWhilst from 'p-whilst';
var PromiseUtils = /** @class */ (function () {
    function PromiseUtils() {
        this.debounce = pDebounce;
        this.delay = pDelay;
        this.filter = pFilter;
        this.forever = pForever;
        this.if = pIf;
        this.limit = pLimit;
        this.locate = pLocate;
        this.map = pMap;
        this.memoize = pMemoize;
        this.pipe = pPipe;
        this.progress = pProgress;
        this.props = pProps;
        this.queue = pQueue;
        this.reduce = pReduce;
        this.reflect = pReflect;
        this.retry = pRetry;
        this.some = pSome;
        this.tap = pTap;
        this.throttle = pThrottle;
        this.time = pTime;
        this.timeout = pTimeout;
        this.times = pTimes;
        this.waitFor = pWaitFor;
        this.waterfall = pWaterfall;
        this.whilst = pWhilst;
        this.errors = {
            some: {
                AggregateError: AggregateError,
                FilterError: FilterError,
            },
        };
    }
    return PromiseUtils;
}());
export { PromiseUtils };
export { pDebounce, pDelay, pFilter, pForever, pIf, pLimit, pLocate, pMap, pMemoize, pPipe, pProgress, pProps, pQueue, pReduce, pReflect, pRetry, pSome, pTap, pThrottle, pTime, pTimeout, pTimes, pWaitFor, pWaterfall, pWhilst, };
export default new PromiseUtils();
