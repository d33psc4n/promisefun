import pDelay from 'delay';
import pDebounce from 'p-debounce';
import pFilter from 'p-filter';
import pForever from 'p-forever';
import pIf from 'p-if';
import pLimit from 'p-limit';
import pLocate from 'p-locate';
import pMap from 'p-map';
import pMemoize from 'p-memoize';
import pPipe from 'p-pipe';
import pProgress from 'p-progress';
import pProps from 'p-props';
import pQueue from 'p-queue';
import pReduce from 'p-reduce';
import pReflect from 'p-reflect';
import pRetry from 'p-retry';
import pSome, { AggregateError, FilterError } from 'p-some';
import pTap from 'p-tap';
import pThrottle from 'p-throttle';
import pTime from 'p-time';
import pTimeout from 'p-timeout';
import pTimes from 'p-times';
import pWaitFor from 'p-wait-for';
import pWaterfall from 'p-waterfall';
import pWhilst from 'p-whilst';
interface SomeErrors {
    AggregateError: typeof AggregateError;
    FilterError: typeof FilterError;
}
interface Errors {
    some: SomeErrors;
}
export declare class PromiseUtils {
    debounce: typeof pDebounce;
    delay: typeof pDelay;
    errors: Errors;
    filter: typeof pFilter;
    forever: typeof pForever;
    if: typeof pIf;
    limit: typeof pLimit;
    locate: typeof pLocate;
    map: typeof pMap;
    memoize: typeof pMemoize;
    pipe: typeof pPipe;
    progress: typeof pProgress;
    props: typeof pProps;
    queue: typeof pQueue;
    reduce: typeof pReduce;
    reflect: typeof pReflect;
    retry: typeof pRetry;
    some: typeof pSome;
    tap: typeof pTap;
    throttle: typeof pThrottle;
    time: typeof pTime;
    timeout: typeof pTimeout;
    times: typeof pTimes;
    waitFor: typeof pWaitFor;
    waterfall: typeof pWaterfall;
    whilst: typeof pWhilst;
    constructor();
}
export { pDebounce, pDelay, pFilter, pForever, pIf, pLimit, pLocate, pMap, pMemoize, pPipe, pProgress, pProps, pQueue, pReduce, pReflect, pRetry, pSome, pTap, pThrottle, pTime, pTimeout, pTimes, pWaitFor, pWaterfall, pWhilst, };
declare const _default: PromiseUtils;
export default _default;
